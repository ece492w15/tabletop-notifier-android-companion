/*
 * Copyright (C) 2015 Alexander Cheung
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.alexandercheung.tabletopnotifier;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class TabletopNotifierService extends NotificationListenerService {

    private static final String LOG_TAG = TabletopNotifierService.class.getName();
    public static final String PACKAGE = "package";
    public static final String APP_NAME = "app";
    public static final String TITLE = "title";
    public static final String TEXT = "text";

    public static final int APP_NAME_SIZE = 25;
    public static final int TITLE_SIZE = 25;
    public static final int TEXT_SIZE = 256;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        JSONObject notification = parseNotification(sbn);
        Intent notifIntent = new Intent("tabletop-notif");
        notifIntent.putExtra("notif", notification.toString());
        sendBroadcast(notifIntent);
    }

    /**
     * Parse a {@link StatusBarNotification} into a {@link JSONObject}.
     * @param sbn the notification to parse.
     * @return the parsed notification in JSON format.
     */
    private JSONObject parseNotification(StatusBarNotification sbn) {
        String pack = sbn.getPackageName();
        Bundle extras = sbn.getNotification().extras;
        String title = extras.getString("android.title");
        CharSequence textCharSequence = extras.getCharSequence("android.text");
        String text = (textCharSequence == null) ? "" : textCharSequence.toString();

        String appName = getAppName(sbn);

        // Log the notification contents
        Log.i(LOG_TAG, PACKAGE + ": " + pack);
        Log.i(LOG_TAG, APP_NAME + ": " + appName);
        Log.i(LOG_TAG, TITLE + ": " + title);
        Log.i(LOG_TAG, TEXT + ": " + text);

        if (appName.length() > APP_NAME_SIZE) {
            appName = appName.substring(0, APP_NAME_SIZE-3) + "...";
        }
        if (title.length() > TITLE_SIZE) {
            title = title.substring(0, TITLE_SIZE-3) + "...";
        }
        if (text.length() > TEXT_SIZE) {
            text= text.substring(0, TEXT_SIZE-3) + "...";
        }

        JSONObject notification = new JSONObject();
        try {
            notification.put(APP_NAME, appName);
            notification.put(TITLE, title);
            notification.put(TEXT, text);
            Log.i(LOG_TAG, notification.toString());
        } catch (JSONException e) {
            Log.e(LOG_TAG, "An error has occurred while packaging notification.", e);
        }
        return notification;
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        Log.i(LOG_TAG, "Notification Removed");
    }

    private String getAppName(StatusBarNotification sbn) {
        final PackageManager packageManager = getApplicationContext().getPackageManager();
        ApplicationInfo applicationInfo;
        try {
            applicationInfo = packageManager.getApplicationInfo(sbn.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            applicationInfo = null;
        }

        if (applicationInfo == null) {
            return "Unknown";
        }
        return packageManager.getApplicationLabel(applicationInfo).toString();
    }
}

/*
 * Copyright (C) 2015 Alexander Cheung
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Furthermore it is derived from the Android Open Source Project examples
 * thus some of the code is Copyright (C) 2014 The Android Open Source
 * Project; All Rights Reserved
 *
 * http://github.com/googlesamples/android-BluetoothChat
 */

package ca.alexandercheung.tabletopnotifier;

/**
 * Defines several constants used between {@link BluetoothCommService} and the UI.
 */
public interface Constants {

    // Message types sent from the BluetoothCommService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothCommService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Command Strings
    public static final String COMMAND_NOTIF = "TabletopNotifierCommand";
    public static final String CMD_SET_TIME = "set_time";

    // Sample App Names
    public static final String SMS_APP = "Messages";
    public static final String EMAIL_APP = "Email";
}
